# Libiconv 三方库说明
## 功能简介
libiconv是一个实现字符集转换的库，用于没有Unicode或无法从其他字符转换为Unicode的系统。
## 使用约束
+ Rom版本：OpenHarmony3.2 beta1
+ 三方库版本：1.17
+ 当前适配的功能：[Unicode与其他编码的相互转换](https://www.gnu.org/software/libiconv/)
+ License：[LGPL 2.1](https://savannah.gnu.org/projects/libiconv)
## 集成方式
+ [系统Rom包集成](docs/rom_integrate.md)